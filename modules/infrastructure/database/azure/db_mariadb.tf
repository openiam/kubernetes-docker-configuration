data "azurerm_mariadb_server" "db" {
  count               = lower(var.context.database.azure.type) == "mariadb" ? 1:0
  name                = azurerm_mariadb_server.db.0.name
  resource_group_name = var.context.resource_group_name
}

provider "mysql" {
  endpoint = lower(var.context.database.azure.type) == "mariadb" ? "${data.azurerm_mariadb_server.db.0.fqdn}:3306" : ""
  username = "${var.context.database.root.user}@${var.context.app_name}"
  password = var.context.database.root.password
  tls      = false
}

resource "azurerm_mariadb_server" "db" {
  count                         = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  name                          = var.context.app_name
  location                      = var.context.region
  resource_group_name           = var.context.resource_group_name
  administrator_login           = var.context.database.root.user
  administrator_login_password  = var.context.database.root.password

  sku_name                      = var.context.database.azure.sku_name
  storage_mb                    = var.context.database.azure.storage_mb
  version                       = "10.3"

  auto_grow_enabled             = true
  backup_retention_days         = var.context.database.azure.backup_retention_days
  geo_redundant_backup_enabled  = false
  public_network_access_enabled = false
  ssl_enforcement_enabled       = false
}

## This configuration is to prevent flyway migrations failing
resource "azurerm_mariadb_configuration" "function_creators" {
  count               = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  resource_group_name = var.context.resource_group_name
  server_name         = azurerm_mariadb_server.db.0.name
  name                = "log_bin_trust_function_creators"
  value               = "ON"
}

resource "azurerm_mariadb_database" "openiam" {
  count               = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  name                = var.context.database.openiam.database_name
  resource_group_name = var.context.resource_group_name
  server_name         = azurerm_mariadb_server.db.0.name
  charset             = "utf8"
  collation           = "utf8_general_ci"
}

resource "azurerm_mariadb_database" "activiti" {
  count               = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  name                = var.context.database.activiti.database_name
  resource_group_name = var.context.resource_group_name
  server_name         = azurerm_mariadb_server.db.0.name
  charset             = "utf8"
  collation           = "utf8_general_ci"
}

resource "mysql_user" "openiam" {
  count              = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  user               = var.context.database.openiam.user
  host               = "%"
  plaintext_password = var.context.database.openiam.password
  depends_on         = [azurerm_private_endpoint.db]
}

resource "mysql_user" "activiti" {
  count              = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  user               = var.context.database.activiti.user
  host               = "%"
  plaintext_password = var.context.database.activiti.password
  depends_on         = [azurerm_private_endpoint.db]  
}

resource "mysql_grant" "useraccess-openiam" {
  count      = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  user       = mysql_user.openiam.0.user
  host       = mysql_user.openiam.0.host
  database   = var.context.database.openiam.database_name
  privileges = ["ALL PRIVILEGES"]
  depends_on = [azurerm_private_endpoint.db]
}

resource "mysql_grant" "useraccess-activiti" {
  count      = lower(var.context.database.azure.type) == "mariadb" ? 1 : 0
  user       = mysql_user.activiti.0.user
  host       = mysql_user.activiti.0.host
  database   = var.context.database.activiti.database_name
  privileges = ["ALL PRIVILEGES"]
  depends_on = [azurerm_private_endpoint.db]
}
