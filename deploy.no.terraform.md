# Deloying OpenIAM to Kubernetes (without Terraform)

This guide describes how to deploy OpenIAM to a private (not AWS, GKE) kubernetes cluster via helm, but without terraform.  This type of deployment is *not* recommended due to the number of helm variables.

If you are deploying to AWS or GKE, please use terraform!

## Prerequisites

Next, follow this guide to enable helm RBAC in your cluster:
```
https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/#use-case-2-enable-helm-in-your-cluster
```

In addition, run the following command to ensure that configmaps are created correctly. *You only need to do this once, or when the underlying files change*

```
mkdir -p openiam-configmap/.ssl && mkdir -p openiam-configmap/.apache && cp -r .ssl/* openiam-configmap/.ssl/ && cp -r .apache/* openiam-configmap/.apache/
```

## Deploying

We provide a working example of how to deploy OpenIAM via helm.  Please see `setup-no-tf.sh`.  This contains all of the required values for each helm chart that we deploy.


## Migration guide

# pre-4.2.1 to 4.2.1

We've updated our vault datastore in 4.2.1.  To migrate, you will have to do the following:

1) Deploy with the `openiam.vault.migrate` terraform variable set to `true`, when deploying `openiam-vault` and `openiam`
2) Wait for all pods to come up
3) Redeploy with the `vault.migrate` terraform variable set to `false`, when deploying `openiam-vault` and `openiam`
4) Wait for all pods to come up
