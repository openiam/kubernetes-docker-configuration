#!/bin/bash

export APP_NAME="stress-testing"

helm uninstall "${APP_NAME}"-configmap
helm uninstall "${APP_NAME}"-redis
helm uninstall "${APP_NAME}-cassandra"
helm uninstall "${APP_NAME}-elasticsearch"
helm uninstall "${APP_NAME}-gremlin"
helm uninstall "${APP_NAME}-rabbitmq"
helm uninstall "${APP_NAME}-database"
helm uninstall "${APP_NAME}-pvc"
helm uninstall "${APP_NAME}-consul"
helm uninstall "${APP_NAME}-vault"
helm uninstall "${APP_NAME}-openiam"
helm uninstall "${APP_NAME}-rproxy"
