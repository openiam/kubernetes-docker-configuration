#!/usr/bin/env bash

set -x

echo "This script will clean up all pods, pvc, pv, and config maps assocaited with openiam."
read -p "Press enter to continue"

./shutdown.sh

sleep 5

helm delete openiam-devops
helm del --purge openiam-devops

helm delete openiam-redis
helm del --purge openiam-redis

helm delete openiam-rabbitmq
helm del --purge openiam-rabbitmq

helm delete openiam-mariadb
helm del --purge openiam-mariadb

helm delete openiam-postgresql
helm del --purge openiam-postgresql

helm delete openiam-elasticsearch
helm del --purge openiam-elasticsearch

kubectl delete pods --all --grace-period=0 --force && kubectl delete pvc --all && kubectl delete pv --all && kubectl delete cm --all