#!/usr/bin/env bash

set -x


. env.sh

if [ -f /opt/openiam/webapps/env.sh ]
then
  . /opt/openiam/webapps/env.sh
fi

helm delete openiam
helm del --purge openiam