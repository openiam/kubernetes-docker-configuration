provider "helm" {
  version = "~> 2.0.0"
  kubernetes {
    config_path            = "~/.kube/config"
    host                   = "${var.kube_host}"
    token                  = "${var.kube_token}"
    client_certificate     = "${var.kube_client_certificate}"
    client_key             = "${var.kube_client_key}"
    cluster_ca_certificate = "${var.kube_cluster_ca_certificate}"
  }
}

resource "null_resource" "depends_on" {
  triggers = {
    depends_on = "${join("", var.depends)}"
  }
}


resource "helm_release" "redis" {
  name      = "${var.context.app_name}-redis"
  repository = "${var.context.mods.bitnami}"
  chart      = "redis"
  namespace = "${var.context.namespace}"
  version    = "18.17.0"
  recreate_pods = true
  wait       = "false"
  depends_on    = [null_resource.depends_on]

  values = [
    "${file(".deploy/redis.values.yaml")}"
  ]

  set {
      name = "openiam.appname"
      value = "${var.context.app_name}"
  }

  set {
    name = "auth.password"
    value = "${var.context.redis.password}"
  }

  set {
    name = "global.redis.password"
    value = "${var.context.redis.password}"
  }

  set {
    name = "replica.replicaCount"
    value = "${var.context.redis.helm.replicas}"
  }

  set {
    name = "sentinel.enabled"
    value = "${var.context.redis.helm.sentinel.enabled}"
  }

  set {
    name = "sentinel.downAfterMilliseconds"
    value = "${var.context.redis.helm.sentinel.downAfterMilliseconds}"
  }

  set {
    name = "sentinel.failoverTimeout"
    value = "${var.context.redis.helm.sentinel.failoverTimeout}"
  }
}
