#!/usr/bin/env bash

#uncomment to debug this script.
#set -x
set -e

. env.sh

#Change this based on your requirements
BUILD_ENVIRONMENT="dev"
OPENIAM_VERSION_NUMBER="4.2.1.12"
OPENIAM_BASH_LOG_LEVEL="warn"
APP_NAME="test2021"

# Change this by demand. Usually this is what you need.
OPENIAM_DB_USERNAME="IAMUSER"
ACTIVITI_DB_USERNAME="ACTIVITI"
OPENIAM_DB_NAME="openiam"
ACTIVITI_DB_NAME="activiti"
IMAGE_PULL_POLICY="Always"
ELASTICSEARCH_USERNAME="elastic"

REDIS_CHART_VERSION="17.3.1"
ELASTICSEARCH_CHART_VERSION="7.17.3"
MARIADB_CHART_VERSION="10.5.1"
CONSUL_CHART_VERSION="0.48.0"
RABBITMQ_CHART_VERSION="10.1.16"
CASSANDRA_CHART_VERSION="9.2.5"

# these are only used if SafeNet Authentication will be used (SAS module)
SAS_FIRST_PROTOCOL="http"
SAS_FIRST_SERVER="localhost"
SAS_FIRST_PORT="80"
SAS_SECOND_PROTOCOL="http"
SAS_SECOND_SERVER=""
SAS_SECOND_PORT="80"

NUM_WORKER_NODES="1"

FLYWAY_COMMAND="migrate"

# these are only used if TLS is enabled in RabbitMQ
export RABBITMQ_HOST=${APP_NAME}-rabbitmq


setlonghorn() {
#Set longhorn default storageclass
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
kubectl get storageclass
}


install() {
#helm repo add stable https://charts.helm.sh/stable --force-update
#helm repo add bitnami https://charts.bitnami.com/bitnami
#helm repo add bigdata-gradiant https://gradiant.github.io/bigdata-charts/
#helm repo add hashicorp https://helm.releases.hashicorp.com
#helm repo add elastic https://helm.elastic.co
helm repo add openiam https://openiam.jfrog.io/artifactory/helm

if helm get manifest "${APP_NAME}"-configmap > /dev/null; then
    echo "Config map ${APP_NAME}-configmap has been already installed. Skip it"
else
    echo "Initialize new configmap ${APP_NAME}-configmap"
    if [ -z "${DOCKERHUB_USERNAME}" ]; then
      echo -n "Enter Username to access container registry: "
      read -r DOCKERHUB_USERNAME
    fi

    if [ -z "${DOCKERHUB_PASSWORD}" ]; then
      echo -n "Enter Password for user ${DOCKERHUB_USERNAME} to access container registry: "
      read -s DOCKERHUB_PASSWORD
      echo ""
    fi
    echo "Creating new Config Maps and Secrets ${APP_NAME}-configmap"
    mkdir -p openiam-configmap/.ssl && mkdir -p openiam-configmap/.apache && cp -r .ssl/* openiam-configmap/.ssl/ && cp -r .apache/* openiam-configmap/.apache/

    REDIS_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    REDIS_SENTINEL_PASSWORD="${REDIS_PASSWORD}"
    sleep 1
    RABBITMQ_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    DB_ROOT_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    JKS_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    JKS_KEY_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    COOKIE_KEY_PASS="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    COMMON_KEY_PASS="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    VAULT_KEY_PASS="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    RABBIT_JKS_KEY_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    OPENIAM_DB_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    ACTIVITI_DB_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    ELASTICSEARCH_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    CASSANDRA_PASSWORD="$(date +%s | sha256sum | base64 | head -c 32 ; echo)"
    sleep 1
    RABBITMQ_USERNAME="openiam"
    sleep 1

    helm install "${APP_NAME}"-configmap ./openiam-configmap \
    --set openiam.database.jdbc.openiam.databaseName="${OPENIAM_DB_NAME}" \
    --set openiam.database.jdbc.activiti.databaseName="${ACTIVITI_DB_NAME}" \
    --set openiam.database.jdbc.openiam.schemaName="${OPENIAM_DB_NAME}" \
    --set openiam.database.jdbc.activiti.schemaName="${ACTIVITI_DB_NAME}" \
    --set openiam.vault.secrets.jdbc.openiam.username="${OPENIAM_DB_USERNAME}" \
    --set openiam.vault.secrets.jdbc.activiti.username="${ACTIVITI_DB_USERNAME}" \
    --set openiam.vault.secrets.jdbc.openiam.password="${OPENIAM_DB_PASSWORD}" \
    --set openiam.vault.secrets.jdbc.activiti.password="${ACTIVITI_DB_PASSWORD}" \
    --set openiam.vault.secrets.jdbc.root.user=root \
    --set openiam.vault.secrets.jdbc.root.password="${DB_ROOT_PASSWORD}" \
    --set openiam.vault.secrets.redis.password="${REDIS_PASSWORD}" \
    --set openiam.vault.secrets.redis.sentinel.password="${REDIS_SENTINEL_PASSWORD}" \
    --set openiam.vault.secrets.rabbitmq.password="${RABBITMQ_PASSWORD}" \
    --set openiam.flyway.openiam.username="${OPENIAM_DB_USERNAME}" \
    --set openiam.flyway.activiti.username="${ACTIVITI_DB_USERNAME}" \
    --set openiam.flyway.openiam.password="${OPENIAM_DB_PASSWORD}" \
    --set openiam.flyway.activiti.password="${ACTIVITI_DB_PASSWORD}" \
    --set openiam.cassandra.password="${CASSANDRA_PASSWORD}" \
    --set openiam.vault.secrets.rabbitmq.username="${RABBITMQ_USERNAME}" \
    --set openiam.rproxy.http=0 \
    --set openiam.vault.secrets.javaKeystorePassword=changeit \
    --set openiam.vault.secrets.jks.password="${JKS_PASSWORD}" \
    --set openiam.vault.secrets.jks.keyPassword="${JKS_KEY_PASSWORD}" \
    --set openiam.vault.secrets.jks.cookieKeyPassword="${COOKIE_KEY_PASS}" \
    --set openiam.vault.secrets.jks.commonKeyPassword="${COMMON_KEY_PASS}" \
    --set openiam.vault.secrets.elasticsearch.username="${ELASTICSEARCH_USERNAME}" \
    --set openiam.vault.secrets.elasticsearch.password="${ELASTICSEARCH_PASSWORD}" \
    --set openiam.vault.keypass="${VAULT_KEY_PASS}" \
    --set openiam.rabbitmq.tls.enabled=true \
    --set openiam.vault.secrets.rabbitmq.jksKeyPassword="${RABBIT_JKS_KEY_PASSWORD}" \
    --set openiam.image.environment="${BUILD_ENVIRONMENT}" \
    --set openiam.image.pullPolicy="${IMAGE_PULL_POLICY}" \
    --set openiam.image.credentials.username="${DOCKERHUB_USERNAME}" \
    --set openiam.image.credentials.password="${DOCKERHUB_PASSWORD}" \
    --set openiam.image.credentials.registry="${DOCKER_REGISTRY}"

    if test $? -eq 0; then
      echo "Config map ${APP_NAME}-configmap initialized."
      echo "Please store the following password. You also can check the values from k8s secret."
      echo "Redis Password: ${REDIS_PASSWORD}"
      echo "RabbitMQ Password: ${RABBITMQ_PASSWORD}"
      echo "Database (MariaDB) Root password: ${DB_ROOT_PASSWORD}"
    else
       echo "Can't continue because  ${APP_NAME}-configmap can't be initialized."
       exit 1
    fi
fi

echo "Setup ${APP_NAME}-redis"
REDIS_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.redisPassword}" | base64 --decode)"
echo "${APP_NAME}-redis not found. Installing"
helm upgrade --install "${APP_NAME}-redis" openiam/redis \
             --version "${REDIS_CHART_VERSION}" \
             -f .deploy/redis.values.yaml \
             --set sentinel.downAfterMilliseconds="5000" \
             --set sentinel.failoverTimeout="5000" \
             --set sentinel.enabled="true" \
             --set auth.password="${REDIS_PASSWORD}" \
             --set global.redis.password="${REDIS_PASSWORD}"

echo "Setup ${APP_NAME}-cassandra"
echo "${APP_NAME}-cassandra not found. Installing"
helm upgrade --install "${APP_NAME}-cassandra" openiam/cassandra \
        -f .deploy/cassandra.values.yaml \
        --version "${CASSANDRA_CHART_VERSION}" \
        --set persistence.size=5Gi \
        --set service.nodePorts.cql=9042 \
        --set dbUser.password="${CASSANDRA_PASSWORD}" \
        --set cluster.replicaCount=1

echo "Setup ${APP_NAME}-elasticsearch"
echo "${APP_NAME}-elasticsearch not found. Installing"
helm upgrade --install "${APP_NAME}"-elasticsearch openiam/elasticsearch \
  --version "${ELASTICSEARCH_CHART_VERSION}" \
  -f .deploy/elasticsearch.values.yaml \
  --set clusterHealthCheckParams="wait_for_status=yellow&timeout=10s" \
  --set-string replicas="${NUM_WORKER_NODES}" \
  --set volumeClaimTemplate.resources.requests.storage=5Gi \
  --set esJavaOpts="-Xmx1536m -Xms1536m"

DOCKERHUB_CREDENTIALS_JSON="$(kubectl get secret globalpullsecret -o jsonpath="{.data.\.dockerconfigjson}")"

echo "Setup ${APP_NAME}-gremlin"
echo "${APP_NAME}-gremlin not found. Installing"
helm upgrade --install "${APP_NAME}"-gremlin ./openiam-gremlin \
      -f .deploy/openiam.gremlin.values.yaml \
      --set openiam.appname="${APP_NAME}" \
      --set openiam.image.prefix="${CONTAINER_INFRA_NAMESPACE}" \
      --set openiam.image.environment="${BUILD_ENVIRONMENT}" \
      --set openiam.image.version="${OPENIAM_VERSION_NUMBER}" \
      --set openiam.image.pullPolicy="${IMAGE_PULL_POLICY}" \
      --set openiam.backend.type="cql" \
      --set openiam.backend.host="${APP_NAME}-cassandra" \
      --set openiam.backend.port=9042 \
      --set openiam.gremlin.additionalJavaOpts="-Xms512m -Xmx768m" \
      --set openiam.cloud_provider="helm" \
      --set openiam.elasticsearch.host=elasticsearch-master \
      --set openiam.elasticsearch.port=9200 \
      --set openiam.gremlin.replicas="${NUM_WORKER_NODES}" \
      --set openiam.image.credentialsJSON="${DOCKERHUB_CREDENTIALS_JSON}" \
      --set openiam.bash.log.level="${OPENIAM_BASH_LOG_LEVEL}" \
      --set openiam.elasticsearch.username="${ELASTICSEARCH_USERNAME}" \
      --set openiam.elasticsearch.password="${ELASTICSEARCH_PASSWORD}"


echo "Setup ${APP_NAME}-rabbitmq"
echo "${APP_NAME}-rabbitmq not found. Installing"
RABBITMQ_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.rabbitmqPassword}" | base64 --decode)"
helm upgrade --install "${APP_NAME}-rabbitmq" openiam/rabbitmq \
  --version "${RABBITMQ_CHART_VERSION}" \
  -f .deploy/rabbitmq.values.yaml \
  --set replicaCount="${NUM_WORKER_NODES}" \
  --set auth.username="${RABBITMQ_USERNAME}" \
  --set auth.password="${RABBITMQ_PASSWORD}" \
  --set memoryHighWatermark.enabled="true" \
  --set memoryHighWatermark.type="absolute" \
  --set memoryHighWatermark.value="1843MB" \
  --set auth.erlangCookie="openiamCoookie" \
  --set extraPlugins="rabbitmq_delayed_message_exchange" \
  --set loadDefinition.enabled="true" \
  --set loadDefinition.existingSecret="rabbitmq-load-definition" \
  --set extraEnvVars[0].name="RABBITMQ_SERVER_ADDITIONAL_ERL_ARGS" \
  --set extraEnvVars[0].value="-rabbitmq_management path_prefix \"/rabbitmq\"" \
  --set extraConfiguration="load_definitions = /app/load_definition.json" \
  --set resources.requests.memory="2048Mi" \
  --set resources.limits.memory="2048Mi" \
  --set service.type=ClusterIP \
  --set auth.tls.sslOptionsVerify=verify_none \
  --set auth.tls.failIfNoPeerCert=false \
  --set auth.tls.enabled=false


echo "Setup ${APP_NAME}-database"
helm upgrade --install "${APP_NAME}-database" openiam/mariadb  \
  -f .deploy/mariadb.values.yaml \
  --version "${MARIADB_CHART_VERSION}" \
  --set auth.rootPassword="${ROOT_PASSWORD}" \
  --set initdbScriptsConfigMap="mariadb-initdbscripts" \
  --set openiam.bash.log.level="warn"


echo "Setup NFS file system ${APP_NAME}-pvc"
helm upgrade --install "${APP_NAME}-pvc" \
             -f ./.deploy/$1.pvc.values.yaml \
             ./openiam-pvc


echo "Setup Consul as ${APP_NAME}-consul"
helm upgrade --install "${APP_NAME}-consul" openiam/consul \
  --version "${CONSUL_CHART_VERSION}" \
  -f ./.deploy/consul.values.yaml \
  --set global.name="${APP_NAME}-consul" \
  --set server.replicas="${NUM_WORKER_NODES}" \
  --set server.storage=5Gi \
  --set server.connect=true \
  --set client.grpc=true


echo "Setup Vault as ${APP_NAME}-vault"
helm upgrade --install "${APP_NAME}-vault" ./openiam-vault \
  -f .deploy/openiam.vault.values.yaml \
  --set openiam.appname="${APP_NAME}" \
  --set openiam.image.prefix="${CONTAINER_INFRA_NAMESPACE}" \
  --set openiam.image.environment="${BUILD_ENVIRONMENT}" \
  --set openiam.image.version="${OPENIAM_VERSION_NUMBER}" \
  --set openiam.image.pullPolicy="${IMAGE_PULL_POLICY}" \
  --set openiam.image.credentialsJSON="${DOCKERHUB_CREDENTIALS_JSON}" \
  --set openiam.bash.log.level="${OPENIAM_BASH_LOG_LEVEL}" \
  --set openiam.vault.migrate=false \
  --set openiam.vault.replicas="${NUM_WORKER_NODES}" \
  --set openiam.vault.url="${APP_NAME}-vault" \
  --set openiam.consul.url="${APP_NAME}-consul-server" \
  --set openiam.consul.port=8500 \
  --set openiam.vault.cert.country=US \
  --set openiam.vault.cert.state=NY \
  --set openiam.vault.cert.locality=NYC \
  --set openiam.vault.cert.organization=OpenIAM \
  --set openiam.vault.cert.organizationunit=DevOps


  OPENIAM_DB_USERNAME="$(kubectl get secret secrets -o jsonpath="{.data.openiamDatabaseUserName}" | base64 --decode)"
  OPENIAM_DB_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.openiamDatabasePassword}" | base64 --decode)"

  OPENIAM_ACTIVITI_USERNAME="$(kubectl get secret secrets -o jsonpath="{.data.activitiDatabaseUserName}" | base64 --decode)"
  OPENIAM_ACTIVITI_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.activitiDatabasePassword}" | base64 --decode)"
  DB_ROOT_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.databaseRootPassword}" | base64 --decode)"
  DB_ROOT_USERNAME="$(kubectl get secret secrets -o jsonpath="{.data.databaseRootUserName}" | base64 --decode)"
  RABBITMQ_PASSWORD="$(kubectl get secret secrets -o jsonpath="{.data.rabbitmqPassword}" | base64 --decode)"


echo "Setup OpenIAM Core as ${APP_NAME}-openiam"

helm upgrade --install "${APP_NAME}-openiam" ./openiam \
    -f .deploy/openiam.values.yaml \
   --set openiam.gremlin.host="${APP_NAME}-janusgraph" \
   --set openiam.gremlin.ssl="false" \
   --set openiam.gremlin.type="janusgraph" \
   --set openiam.cloud_provider="helm" \
   --set openiam.java.additional.args.global="-Dlogging.level.root=WARN -Dlogging.level.org.openiam=WARN  -Dlogging.level.org.elasticsearch.client=ERROR" \
   --set openiam.ui.javaOpts="-Djdk.tls.client.protocols=TLSv1.2 -Dorg.openiam.docker.ui.container.name=${APP_NAME}-ui" \
   --set openiam.esb.javaOpts="" \
   --set openiam.idm.javaOpts="" \
   --set openiam.synchronization.javaOpts="" \
   --set openiam.groovy_manager.javaOpts="" \
   --set openiam.business_rule_manager.javaOpts="" \
   --set openiam.workflow.javaOpts="" \
   --set openiam.authmanager.javaOpts="" \
   --set openiam.connectors.ldap.javaOpts="" \
   --set openiam.connectors.google.javaOpts="" \
   --set openiam.connectors.salesforce.javaOpts="" \
   --set openiam.connectors.rexx.javaOpts="" \
   --set openiam.emailmanager.javaOpts="" \
   --set openiam.devicemanager.javaOpts="" \
   --set openiam.sasmanager.javaOpts="" \
   --set openiam.connectors.jdbc.javaOpts="" \
   --set openiam.connectors.saps4hana.javaOpts="" \
   --set openiam.connectors.tableau.javaOpts="" \
   --set openiam.bash.log.level="${OPENIAM_BASH_LOG_LEVEL}" \
   --set openiam.appname="${APP_NAME}" \
   --set openiam.image.environment="${BUILD_ENVIRONMENT}" \
   --set openiam.image.pullPolicy="${IMAGE_PULL_POLICY}" \
   --set openiam.image.prefix="${CONTAINER_SERVICE_NAMESPACE}" \
   --set openiam.image.prefix_infra="${CONTAINER_INFRA_NAMESPACE}" \
   --set openiam.image.version="${OPENIAM_VERSION_NUMBER}" \
   --set openiam.image.credentialsJSON="${DOCKERHUB_CREDENTIALS_JSON}" \
   --set openiam.image.credentials.registry="${DOCKER_REGISTRY}" \
   --set openiam.database.jdbc.openiam.host="${APP_NAME}-database-mariadb" \
   --set openiam.database.jdbc.hibernate.dialect="org.hibernate.dialect.MySQLDialect" \
   --set openiam.flyway.baselineVersion="2.3.0.0" \
   --set openiam.flyway.command=${FLYWAY_COMMAND} \
   --set openiam.database.jdbc.openiam.port=3306 \
   --set openiam.database.jdbc.activiti.host="${APP_NAME}-database-mariadb" \
   --set openiam.database.jdbc.activiti.port=3306 \
   --set openiam.vault.url="${APP_NAME}-vault" \
   --set openiam.vault.secrets.redis.password="${REDIS_PASSWORD}" \
   --set openiam.vault.secrets.redis.sentinel.password="${REDIS_SENTINEL_PASSWORD}" \
   --set openiam.redis.host="${APP_NAME}-redis-headless" \
   --set openiam.redis.port="26379" \
   --set openiam.vault.secrets.jdbc.openiam.username="${OPENIAM_DB_USERNAME}" \
   --set openiam.vault.secrets.jdbc.openiam.password="${OPENIAM_DB_PASSWORD}" \
   --set openiam.vault.secrets.jdbc.activiti.username="${OPENIAM_ACTIVITI_USERNAME}" \
   --set openiam.vault.secrets.jdbc.activiti.password="${OPENIAM_ACTIVITI_PASSWORD}" \
   --set openiam.database.type="MariaDB" \
   --set openiam.database.jdbc.openiam.databaseName="${OPENIAM_DB_NAME}" \
   --set openiam.database.jdbc.activiti.databaseName="${ACTIVITI_DB_NAME}" \
   --set openiam.database.jdbc.openiam.schemaName="${OPENIAM_DB_NAME}" \
   --set openiam.database.jdbc.activiti.schemaName="${ACTIVITI_DB_NAME}" \
   --set openiam.elasticsearch.helm.curate.days=7 \
   --set openiam.elasticsearch.helm.curate.maxIndexDays=14 \
   --set openiam.elasticsearch.helm.curate.sizeGB=2 \
   --set openiam.postgresql.debugclient.enabled="0" \
   --set openiam.ui.replicas="${NUM_WORKER_NODES}" \
   --set openiam.esb.replicas="${NUM_WORKER_NODES}" \
   --set openiam.business_rule_manager.replicas="${NUM_WORKER_NODES}" \
   --set openiam.reconciliation.replicas="${NUM_WORKER_NODES}" \
   --set openiam.idm.replicas="${NUM_WORKER_NODES}" \
   --set openiam.synchronization.replicas="${NUM_WORKER_NODES}" \
   --set openiam.groovy_manager.replicas="${NUM_WORKER_NODES}" \
   --set openiam.workflow.replicas="${NUM_WORKER_NODES}" \
   --set openiam.elasticsearch.host="elasticsearch-master" \
   --set openiam.elasticsearch.port=9200 \
   --set openiam.vault.secrets.jdbc.root.user="${DB_ROOT_USERNAME}" \
   --set openiam.vault.secrets.jdbc.root.password="${DB_ROOT_PASSWORD}" \
   --set openiam.flyway.openiam.username="${OPENIAM_DB_USERNAME}" \
   --set openiam.flyway.activiti.username="${OPENIAM_ACTIVITI_USERNAME}" \
   --set openiam.flyway.openiam.password="${OPENIAM_DB_PASSWORD}" \
   --set openiam.flyway.activiti.password="${OPENIAM_ACTIVITI_PASSWORD}" \
   --set openiam.rabbitmq.host="${APP_NAME}-rabbitmq" \
   --set openiam.vault.secrets.rabbitmq.password="${RABBITMQ_PASSWORD}" \
   --set openiam.redis.debugclient.enabled=0 \
   --set openiam.mysql.debugclient.enabled=0 \
   --set-string openiam.rabbitmq.port=5672 \
   --set openiam.vault.migrate=false \
   --set openiam.rabbitmq.tls.enabled=false \
   --set openiam.authmanager.replicas="${NUM_WORKER_NODES}" \
   --set openiam.emailmanager.replicas="${NUM_WORKER_NODES}" \
   --set openiam.devicemanager.replicas="${NUM_WORKER_NODES}" \
   --set openiam.sasmanager.replicas=0 \
   --set openiam.connectors.ldap.replicas="${NUM_WORKER_NODES}" \
   --set openiam.connectors.google.replicas=0 \
   --set openiam.connectors.salesforce.replicas=0 \
   --set openiam.connectors.aws.replicas=0 \
   --set openiam.connectors.freshdesk.replicas=0 \
   --set openiam.connectors.linux.replicas="${NUM_WORKER_NODES}" \
   --set openiam.connectors.oracle_ebs.replicas=0 \
   --set openiam.connectors.oracle.replicas=0 \
   --set openiam.connectors.scim.replicas="${NUM_WORKER_NODES}" \
   --set openiam.connectors.script.replicas="${NUM_WORKER_NODES}" \
   --set openiam.redis.mode="sentinel" \
   --set openiam.elasticsearch.helm.index.days="10" \
   --set openiam.elasticsearch.helm.index.maxIndexDays="1" \
   --set openiam.elasticsearch.helm.index.sizeGB="10" \
   --set openiam.elasticsearch.helm.index.warnPhaseDays="2" \
   --set openiam.elasticsearch.helm.index.coldPhaseDays="3"

echo "Setup Reverse Proxy"
helm upgrade --install "${APP_NAME}-rproxy" ./openiam-rproxy \
  -f .deploy/openiam.rproxy.values.yaml \
  --set openiam.bash.log.level=warn \
  --set openiam.appname="${APP_NAME}" \
  --set openiam.rproxy.http=1 \
  --set openiam.image.environment="${BUILD_ENVIRONMENT}" \
  --set openiam.image.pullPolicy="${IMAGE_PULL_POLICY}" \
  --set openiam.image.prefix="${CONTAINER_SERVICE_NAMESPACE}" \
  --set openiam.image.version="${OPENIAM_VERSION_NUMBER}" \
  --set openiam.image.credentialsJSON="${DOCKERHUB_CREDENTIALS_JSON}" \
  --set openiam.image.credentials.registry="${DOCKER_REGISTRY}" \
  --set openiam.rproxy.defaultUri=/selfservice/ \
  --set-string openiam.rproxy.disableConfigure=0 \
  --set-string openiam.rproxy.deflate=6 \
  --set-string openiam.rproxy.csp=0 \
  --set-string openiam.rproxy.cors=1 \
  --set-string openiam.rproxy.verbose=0 \
  --set-string openiam.rproxy.debug.base=0 \
  --set-string openiam.rproxy.debug.esb=0 \
  --set-string openiam.rproxy.debug.auth=0 \
  --set-string openiam.rproxy.replicas="${NUM_WORKER_NODES}" \
  --set openiam.rproxy.ssl.cert=openiam.crt \
  --set openiam.rproxy.ssl.certKey=openiam.key \
  --set openiam.rproxy.https.host= \
  --set openiam.rproxy.log.error= \
  --set openiam.rproxy.log.access= \
  --set openiam.ui.service.host="${APP_NAME}-ui" \
  --set openiam.ui.service.port=8080 \
  --set openiam.esb.service.host="${APP_NAME}-esb" \
  --set openiam.esb.service.port=9080
}
#ports
#for Dev purposes do this tho proxy port to you 127.0.0.1 localhost
#kubectl port-forward --namespace default svc/${APP_NAME}-database-mariadb 3306 &
#kubectl port-forward --namespace default svc/${APP_NAME}-vault 8200 &
#kubectl port-forward --namespace default svc/${APP_NAME}-esb 9080 &
#kubectl port-forward --namespace default svc/${APP_NAME}-redis-master 6379 &

PS3='Please enter your choice: '
options=("Openiam nfs pvc provider" "Longhorn pvc provider" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Openiam nfs pvc provider")
            #install dashboard
            kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml
            echo "Perform: "
            echo "kubectl proxy"
            echo "In separate terminal window and hit in the browser:"
            echo "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"
            install openiam
            break
            ;;
        "Longhorn pvc provider")
            setlonghorn
            install longhorn
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
