#!/usr/bin/env bash

set -x

preflight_check () {
  #helm repo remove gradiant && helm repo remove gradient
  #helm repo add stable https://charts.helm.sh/stable --force-update
  #helm repo add bitnami https://charts.bitnami.com/bitnami
  #helm repo add bigdata-gradiant https://gradiant.github.io/bigdata-charts/
  #helm repo add hashicorp https://helm.releases.hashicorp.com
  helm repo add openiam https://openiam.jfrog.io/artifactory/helm

  set -e

  cd ./openiam-pvc
  helm dependency update
  cd ../

  . env.sh

  if [ -f /opt/openiam/webapps/env.sh ]
  then
  . /opt/openiam/webapps/env.sh
  fi

  mkdir -p .deploy
  envsubst < mariadb.values.yaml > .deploy/mariadb.values.yaml
  envsubst < postgresql.values.yaml > .deploy/postgresql.values.yaml
  envsubst < redis.values.yaml > .deploy/redis.values.yaml
  envsubst < stash.values.yaml > .deploy/stash.values.yaml
  envsubst < openiam/values.yaml > .deploy/openiam.values.yaml
  envsubst < openiam-pvc/values.yaml > .deploy/openiam.pvc.values.yaml
  envsubst < openiam-rproxy/values.yaml > .deploy/openiam.rproxy.values.yaml
  envsubst < openiam-gremlin/values.yaml > .deploy/openiam.gremlin.values.yaml
  envsubst < openiam-vault/values.yaml > .deploy/openiam.vault.values.yaml
  envsubst < openiam-configmap/values.yaml > .deploy/openiam.configmap.values.yaml
  envsubst < kibana.values.yaml > .deploy/kibana.values.yaml
  envsubst '${CONTAINER_INFRA_NAMESPACE} ${DOCKER_REGISTRY_SEPARATOR} ${DOCKER_REGISTRY} ${BUILD_ENVIRONMENT} ${OPENIAM_VERSION_NUMBER} ${IMAGE_PULL_POLICY} ${OSS}' < consul.values.yaml > .deploy/consul.values.yaml
  envsubst < elasticsearch.values.yaml > .deploy/elasticsearch.values.yaml
  envsubst '${CONTAINER_INFRA_NAMESPACE} ${DOCKER_REGISTRY_SEPARATOR} ${DOCKER_REGISTRY} ${BUILD_ENVIRONMENT} ${OPENIAM_VERSION_NUMBER} ${IMAGE_PULL_POLICY} ${OSS}' < rabbitmq.values.yaml > .deploy/rabbitmq.values.yaml
  envsubst < cassandra.values.yaml > .deploy/cassandra.values.yaml
  envsubst '${CONTAINER_INFRA_NAMESPACE} ${DOCKER_REGISTRY_SEPARATOR} ${DOCKER_REGISTRY} ${BUILD_ENVIRONMENT} ${OPENIAM_VERSION_NUMBER} ${IMAGE_PULL_POLICY} ${OSS}' < filebeat.values.yaml > .deploy/filebeat.values.yaml
  envsubst '${CONTAINER_INFRA_NAMESPACE} ${DOCKER_REGISTRY_SEPARATOR} ${DOCKER_REGISTRY} ${BUILD_ENVIRONMENT} ${OPENIAM_VERSION_NUMBER} ${IMAGE_PULL_POLICY} ${OSS}' < metricbeat.values.yaml > .deploy/metricbeat.values.yaml

  if [ ! -f .elasticsearch/elasticsearch.key ]; then
    ./generate.elasticsearch.certs.sh
  fi
}

disable_nfs_deployment () {
  sed -i "s/replicaCount: 1/replicaCount: 0/g" .deploy/openiam.pvc.values.yaml > .deploy/openiam.pvc.values.yaml
  sed -i "s/create: true/create: false/g" .deploy/openiam.pvc.values.yaml > .deploy/openiam.pvc.values.yaml
}

longhorn_pvc () {
  # configure terraform.tfvars to use longhorn pvc
  sed -i "s/use_longhorn = false/use_longhorn = true/g" terraform.tfvars

  # configure longhorn.pvc.values.yaml
  sed "s/'nfs'$/'longhorn'/g" .deploy/openiam.pvc.values.yaml > .deploy/longhorn.pvc.values.yaml
  sed -i "s/replicaCount: 1/replicaCount: 0/g" .deploy/longhorn.pvc.values.yaml
  sed -i "s/create: true/create: false/g" .deploy/longhorn.pvc.values.yaml
}

runasroot() {
  # configure `openiam/values.yaml` and `openiam-vault/values.yaml` and set the securityContext to run as root:
  sed -i "s/runAsUser: null/runAsUser: 0/g" .deploy/openiam.values.yaml
  sed -i "s/runAsUser: null/runAsUser: 0/g" .deploy/openiam.vault.values.yaml
}


PS3='Please enter your choice: '
options=("Use openiam nfs pvc provider" "Use longhorn pvc provider" "Use own storage class" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Use openiam nfs pvc provider")
            preflight_check
            break
            ;;
        "Use longhorn pvc provider")
            preflight_check
            longhorn_pvc
            runasroot
            break
            ;;
        "Use own storage class")
            preflight_check
#            runasroot
            disable_nfs_deployment
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
