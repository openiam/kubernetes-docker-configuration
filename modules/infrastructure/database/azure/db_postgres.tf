data "azurerm_postgresql_server" "db" {
  count               = lower(var.context.database.azure.type) == "postgres" ? 1:0
  name                = azurerm_postgresql_server.db.0.name
  resource_group_name = var.context.resource_group_name
}

provider "postgresql" {
  endpoint = lower(var.context.database.azure.type) == "postgres" ? "${data.azurerm_postgresql_server.db.0.fqdn}:5432" : ""
  username = "${var.context.database.root.user}@${var.context.app_name}"
  password = var.context.database.root.password
  tls      = false
}

resource "azurerm_postgresql_server" "db" {
  count                         = lower(var.context.database.azure.type) == "postgres" ? 1 : 0
  name                          = var.context.app_name
  location                      = var.context.region
  resource_group_name           = var.context.resource_group_name

  administrator_login           = var.context.database.root.user
  administrator_login_password  = var.context.database.root.password

  sku_name                      = var.context.database.azure.sku_name
  version                       = "11"
  storage_mb                    = var.context.database.azure.storage_mb

  backup_retention_days         = var.context.database.azure.backup_retention_days
  geo_redundant_backup_enabled  = false
  auto_grow_enabled             = true

  public_network_access_enabled = false
  ssl_enforcement_enabled       = false
}

resource "azurerm_postgresql_database" "openiam" {
  count               = lower(var.context.database.azure.type) == "postgres" ? 1 : 0
  name                = var.context.database.openiam.database_name
  resource_group_name = var.context.resource_group_name
  server_name         = azurerm_postgresql_server.db.0.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "azurerm_postgresql_database" "activiti" {
  count               = lower(var.context.database.azure.type) == "postgres" ? 1 : 0
  name                = var.context.database.activiti.database_name
  resource_group_name = var.context.resource_group_name
  server_name         = azurerm_postgresql_server.db.0.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}

resource "local_file" "postgres_init" {
  count    = lower(var.context.database.azure.type) == "postgres" ? 1 : 0
  content  = local.pg_init_script
  filename = "/tmp/init.sql"
}

resource "null_resource" "postgres_init" {
 count    = lower(var.context.database.azure.type) == "postgres" ? 1 : 0
 triggers = {
   server_name = local.server_name
   always_run = "${timestamp()}"
 }
 provisioner "local-exec" {
    command = "psql --host=$SERVER_NAME --port=5432 --username=$ADMIN_NAME@$SERVER_NAME --dbname=postgres --file=/tmp/init.sql"
    environment = {
      PGPASSWORD = var.context.database.root.password
      SERVER_NAME = local.server_name
      ADMIN_NAME = var.context.database.root.user
    }
  }
 depends_on = [ azurerm_private_endpoint.db ]
}

