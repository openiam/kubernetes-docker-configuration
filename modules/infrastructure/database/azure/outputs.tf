output "database_port" {
  value       = "${lower(var.context.database.azure.type) == "postgres" ? "5432" : lower(var.context.database.azure.type) == "mariadb"  ? "3306" : "1433"}"
  description = "The URI of the created resource"
}

output "database_host" {
  value       = "${lower(var.context.database.azure.type) == "postgres" ? data.azurerm_postgresql_server.db.0.fqdn : lower(var.context.database.azure.type) == "mariadb" ? data.azurerm_mariadb_server.db.0.fqdn : data.azurerm_mssql_server.db.0.fully_qualified_domain_name}"
  description = "The URI of the created resource"
}

output "database_name" {
  value       = ""
  description = "No database was created during spinup of the pod, so returning nothing"
}

output "private_database_link_name" {
  value = data.azurerm_private_dns_zone.db.name
}
