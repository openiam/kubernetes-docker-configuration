data "azurerm_mssql_server" "db" {
  count = lower(var.context.database.azure.type) == "mssql" ? 1:0
  name                = azurerm_mssql_server.db.0.name
  resource_group_name = var.context.resource_group_name
}

resource "azurerm_mssql_server" "db" {
  count                         = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  name                          = var.context.app_name
  resource_group_name           = var.context.resource_group_name
  location                      = var.context.region
  version                       = "12.0"
  administrator_login           = var.context.database.root.user
  administrator_login_password  = var.context.database.root.password
  minimum_tls_version           = "1.2"
  public_network_access_enabled = false
}

resource "azurerm_mssql_database" "openiam" {
  count          = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  name           = var.context.database.openiam.database_name
  server_id      = azurerm_mssql_server.db.0.id
  collation      = "SQL_Latin1_General_CP1_CI_AS"
  license_type   = "LicenseIncluded"
  max_size_gb    = 20
  read_scale     = false
  sku_name       = var.context.database.azure.sku_name
  zone_redundant = true
}

resource "azurerm_mssql_database" "activiti" {
  count          = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  name           = var.context.database.activiti.database_name
  server_id      = azurerm_mssql_server.db.0.id
  collation      = "SQL_Latin1_General_CP1_CI_AS"
  license_type   = "LicenseIncluded"
  max_size_gb    = 20
  read_scale     = false
  sku_name       = var.context.database.azure.sku_name
  zone_redundant = true
}


resource "local_file" "mssql_init_master" {
  count    = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  content  = local.ms_init_master_script
  filename = "/tmp/init_master.sql"
}

resource "local_file" "mssql_init_openiam" {
  count    = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  content  = local.ms_init_openiam_script
  filename = "/tmp/init_openiam.sql"
}

resource "local_file" "mssql_init_activiti" {
  count    = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
  content  = local.ms_init_activiti_script
  filename = "/tmp/init_activiti.sql"
}

resource "null_resource" "mssql_init" {
 count    = lower(var.context.database.azure.type) == "mssql" ? 1 : 0
 triggers = {
   server_name = local.server_name
   always_run = "${timestamp()}"
 }

# need to wait some time to give private endpoint the time to be activated to avoid TF errors on the following sqlcmd commands
 provisioner "local-exec" {
    command = "sleep 180"
  }

 provisioner "local-exec" {
    command = "/opt/mssql-tools/bin/sqlcmd -S $SERVER_NAME -U $ADMIN_NAME -P $ADMIN_PASSWORD -i /tmp/init_master.sql"
    environment = {
      ADMIN_PASSWORD = var.context.database.root.password
      SERVER_NAME = local.server_name
      ADMIN_NAME = var.context.database.root.user
    }
  }

 provisioner "local-exec" {
    command = "/opt/mssql-tools/bin/sqlcmd -S $SERVER_NAME -U $ADMIN_NAME -P $ADMIN_PASSWORD -d $MSSQL_DB -i /tmp/init_openiam.sql"
    environment = {
      ADMIN_PASSWORD = var.context.database.root.password
      SERVER_NAME = local.server_name
      ADMIN_NAME = var.context.database.root.user
      MSSQL_DB = var.context.database.openiam.database_name
    }
  }

 provisioner "local-exec" {
    command = "/opt/mssql-tools/bin/sqlcmd -S $SERVER_NAME -U $ADMIN_NAME -P $ADMIN_PASSWORD -d $MSSQL_DB -i /tmp/init_activiti.sql"
    environment = {
      ADMIN_PASSWORD = var.context.database.root.password
      SERVER_NAME = local.server_name
      ADMIN_NAME = var.context.database.root.user
      MSSQL_DB = var.context.database.activiti.database_name
    }
  }
 depends_on = [ azurerm_private_endpoint.db ]
}
