# Deploying OpenIAM to Kubernetes

This document describes how to install OpenIAM in a kubernetes environment.

## Set Docker environment variables

First, create an account on [Dockerhub](https://hub.docker.com/)

Then export the following environment variables:
```
export DOCKER_REGISTRY=confidant-bhaskara.container-registry.com
export DOCKERHUB_USERNAME=******
export DOCKERHUB_PASSWORD=******
```

Replacing them with their corresponding values

## Install Helm

1. Install helm **v3.3.4**

```
https://github.com/helm/helm/releases/tag/v3.3.4
```

For linux:
```
1) Download https://get.helm.sh/helm-v3.3.4-linux-amd64.tar.gz
2 Unpack it (tar -zxvf helm-v3.3.4-linux-amd64.tar.gz)
3) Find the helm binary in the unpacked directory, and move it to its desired destination (mv linux-amd64/helm /usr/local/bin/helm)
```


## Install Terraform:

1) Install terraform **0.12.31**

```
https://releases.hashicorp.com/terraform/0.12.31/
```

For linux:
```
1) wget https://releases.hashicorp.com/terraform/0.12.31/terraform_0.12.31_linux_amd64.zip
2) unzip terraform_0.12.31_linux_amd64.zip
3) sudo mv terraform /usr/local/bin/terraform
```

## Kubernetes Version

We have tested our terraform and helm scripts with kubernetes **1.27**.  Please use this version.


## Prerequisites

### 1) Vault

We use Vault in order to securely store secrets.  
Our applications use certificate based authentication in order to securely talk to Vault.
Our images will generate a self-signed certificate to be used for vault.  However, if you would like to use
your own certificate, you can do the following:

1) Get a public/private keypair from a valid CA Authority, and put the files in:
* .vault/vault.key - the private key
* .vault/vault.crt - the public key

2) Then, run the following command:
```
. env.sh
openssl pkcs12 -export -in .vault/vault.crt -inkey .vault/vault.key -out .vault/vault.jks -password pass:${VAULT_KEYPASS}
```

a) If you are using terraform, make sure that ${VAULT_KEYPASS} above matches `vault.vaultKeyPassword` in `terraform.tfvars`

b) Also, put all of the resulting files in `openiam-configmap/.vault/`

i.e.
```
cp .vault/vault.key openiam-configmap/.vault/
cp .vault/vault.crt openiam-configmap/.vault/
cp .vault/vault.jks openiam-configmap/.vault/
```

### 2) SMTP Server

You will need to setup an SMTP server.  If running in AWS, you use SES, which is very simple to setup.
If you do not have a corporate SMTP server, there are numerous SMTP Cloud Servers which you can use.
Setting up SMTP is outside the scope of this document.

### 3) RabbitMQ TLS

You can optionally run RabbitMQ with TLS enabled.

#### Adding your own TLS Certificates to RabbitMQ

If you would like to use your own Certificates with RabbitMQ, you will need to get a public/private keypair from a valid CA authority, and generate a JKS file.
Please follow the instructions in the [RabbitMQ TLS README](.rabbitmq/README.md)

#### Generating a self-signed certificate

You can also generate a self-signed certificate by running
```
./generate.rabbitmq.certs.sh
```

### 4) HTTPS certificates

When running in kubernetes, we expose port 80, and 443 if https is enabled.  Our apache httpd server listens to these ports.
To setup https, see our [SSL README](.ssl/README.md) for a list of required files.

### 5) Set required values in env.sh

1) Set the APP_NAME variable in env.sh to a unique string.  This is a unique identifier, and will not be seen by end users.

2) If you're running in AWS, set the `OSS` variable in `env.sh` to `-oss`

### 6) Configure Extra VHost and Apache Configs

You can optionally add 'extra' vhost and apache configs.  To do that, simply modify the following files as needed:
a) .apache/extraVHost.conf
b) .apache/extraApache.conf

These files will be put in /usr/local/apache2/conf/add, in the rproxy pod


#### 6) Initialize and Setup


Run the setup script

```
./setup.sh
```


## RabbitMQ

### Exposing externally

To expose the RabbitMQ Service externally, uncomment the following lines in  `rabbitmq.values.yaml`:

```
# Uncomment to expose externally
# service:
#   type: LoadBalancer
#   clusterIP: None
```

Note that you will have to re-run `setup.sh` after doing this.

## Deploying with Terraform

You should deploy to kubernetes using terraform.  This is the recommended approach.  The guide to do this is [here](deploy.terraform.md)

## Deploying without Terraform

You can deploy without using terraform, but this method is much more complex, requires much more configuration, and is not recommended.  The guide to do this is [here](deploy.no.terraform.md)


### Confirming successful deployment

Confirm that all pods are up and running with the following command:

```
kubectl get pods
```

Ensure that the READY column does not have any failed pods.  For example:

#### Example of running pod
```
test100-esb-0                         1/1     Running   0          2m3s
```

#### Example of failed pod
```
test100-esb-0                         0/1     CrashLoopBackOff   4          2m3s
```

### Debugging failed pods

If a certain pod fails, gather it's logs for analysis.

```
kubectl logs pods/<name_of_failed_pod>
```


## Accessing your deployed instance

To acess your deployed instance of OpenIAM, run the following command:

```
. env.sh
kubectl get "service/${APP_NAME}-rproxy"
```

The output of the above command will contain an EXTERNAL IP column, for example:

```
NAME                     TYPE           CLUSTER-IP     EXTERNAL-IP                                                              PORT(S)        AGE
test100-rproxy   LoadBalancer   172.20.27.78   a0375c89dd2ec11e98bca0648c64953f-439827441.us-west-2.elb.amazonaws.com   80:32468/TCP   3m57s
```

Curl the above URL:
```
curl -L "http://${EXTERNAL_IP_FROM_ABOVE}/webconsole"
```

You may want to add a CNAME alias for the above URL, to make it more human-readable.


## Switching Between Environments/Clients.

We provide a convenience shell script, which saves off terraform files to a specific location, or copies terraform files from that location to this project.

### Example on Saving files

This will save the terraform files from this project into /tmp/client_name/prod

```
./terraform.client.sh save /tmp client_name prod
```


### Example on Using files

This will use the terraform files from /tmp/client_name/prod

```
./terraform.client.sh use /tmp client_name prod
```



# Upgrade Instructions

This document describes how to upgrade from one version to another.

## Upgrading to 4.2.1.2

In 4.2.1.2, we made volume size for mariadb, elasticsearch, and hbase configurable, so you will have to run the following before running terraform

```
1118  kubectl delete statefulset/<app_name>-mariadb-primary
1119  kubectl delete statefulset/<app_name>-mariadb-secondary
1122  kubectl delete deployment/<app_name>-hbase-hdfs-httpfs
1126  kubectl delete StatefulSet/<app_name>-hbase-hdfs-datanode
1128  kubectl delete deployment/<app_name>-metricbeat-metricbeat-metrics
1130  kubectl delete statefulset/<app_name>-hbase-hdfs-namenode
1132  kubectl delete statefulset/<app_name>-hbase-zookeeper
```

## Upgrading to 4.2.1.3


### Change 1 - Update of Kubernetes

We have tested our deployment on 1.23.  If you are using AWS, see the section on updating to 4.2.1.3 in the corresponding (README)[module/core/aws/README.md]

### Change 2 - Stash - We've upated stash, and it is incompatible with previous version of stash. so you will need to undeploy it first

If you are not using stash, you can ignore this section


1) `terraform state rm $(terraform state list  | grep stash | awk '{print $1}')`
2) `helm delete $(helm list | grep stash | awk '{print $1}')`
3) Create a license file, as per the instructions here: https://stash.run/docs/v2022.09.29/setup/install/community/
4) Put the license file in `.stash/licence.txt`


### Change 3 - Elasticsearch

Certain elasticsearch documents can grow to be many GB in size, and thus we must add the possibility to curate them.

Before updating, you will need to run:

5) run the following SQL query:
```
select LOGIN as login, NAME as 'managed_system_name' from LOGIN l
LEFT join MANAGED_SYS ms on ms.MANAGED_SYS_ID=l.MANAGED_SYS_ID
where PROV_STATUS in ('PENDING_CREATE', 'PENDING_UPDATE','PENDING_DISABLE','PENDING_ENABLE','PENDING_DELETE')
```

Note the logins and users.

6) Run the following commands in the esb pod (i.e. `kubectl exec -it $(kubectl get pods | grep esb | awk '{print $1}') bash`)

```
# taken from terraform.tfvars
export ELASTICSEARCH_USERNAME=elastic
export ELASTICSEARCH_PASSWORD=ChangeMeToSomethingMoreSecure123#51

AUTHORIZATION_HEADER=""
if [ ! -z "$ELASTICSEARCH_USERNAME" ] && [ "$ELASTICSEARCH_USERNAME" != "null" ] && [ ! -z "$ELASTICSEARCH_PASSWORD" ] && [ "$ELASTICSEARCH_PASSWORD" != "null" ]; then
AUTHORIZATION_HEADER="Authorization: Basic $(echo -n ${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD} | base64)"
fi
for indexToCurate in provisionrequest connectorreply provisionconnectorrequest; do
  curl -H "${AUTHORIZATION_HEADER}" -XDELETE "http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/$indexToCurate*"
done
```

Example:
```
lbornov2@mypc kubernetes-docker-configuration % kubectl get pods | grep esb
test2021-esb-0                                  1/1     Running     3          13h
lbornov2@mypc kubernetes-docker-configuration % kubectl exec -it test2021-esb-0 bash
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.
bash-5.1$ export ELASTICSEARCH_USERNAME=elastic
export ELASTICSEARCH_PASSWORD=ChangeMeToSomethingMoreSecure123#51

AUTHORIZATION_HEADER=""
if [ ! -z "$ELASTICSEARCH_USERNAME" ] && [ "$ELASTICSEARCH_USERNAME" != "null" ] && [ ! -z "$ELASTICSEARCH_PASSWORD" ] && [ "$ELASTICSEARCH_PASSWORD" != "null" ]; then
AUTHORIZATION_HEADER="Authorization: Basic $(echo -n ${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD} | base64)"
fi
for indexToCurate in provisionrequest connectorreply provisionconnectorrequest; do
  curl -H "${AUTHORIZATION_HEADER}" -XDELETE "http://${ELASTICSEARCH_HOST}:${ELASTICSEARCH_PORT}/$indexToCurate*"
done
{"acknowledged":true}
{"acknowledged":true}
{"acknowledged":true}
```

### Change 4 - NFS

In addition, we've updated the size of our NFS volume from 2G to 4G.  Thus, please do the following:

7) `kubectl get storageclass`
8) Pick the (non-nfs) storageclass.  For example, `gp2` (if using AWS)
9) `kubectl edit storageclass gp2`
  Add: `allowVolumeExpansion: true`
  If it doesn't already exist.
10) `kubectl get pvc | grep nfs-server`
11) `kubectl edit pvc <nfs-pvc-from-above>``
  Edit the storage size
12) `kubectl get sts | grep nfs`
13) `kubectl delete statefulset/<statefulset_from_above>`
14) `kubectl get sts | grep rabbitmq`
15) `kubectl delete statefulset/<statefulset_from_above>`, for example `kubectl delete statefulset/test2021-rabbitmq`
16) `kubectl delete jobs --all`
17) `kubectl delete cronjobs --all`


### Change 5 - Redeploy

Now that you've finished the manual steps, you can redeploy

18) `./setup.sh`
19) `terraform init && terraform apply`, or re-run helm script


### Change 6 - Fix Flyway -

We've fixed old flyway scripts to run on newer database versions.  As a result, flyway will fail to complete successfully

20) Repair Flyway
- If using terraform, set `database.flywayCommand` to `repair`, then re-run `terraform apply`
- If using helm, set the `FLYWAY_COMMAND` ENV variable in the deployment script to `repair`, and re-apply helm

22) Migrate Flyway
- If using terraform, set `database.flywayCommand` to `migrate`, then re-run `terraform apply`
- If using helm, set the `FLYWAY_COMMAND` ENV variable in the deployment script to `migrate`, and re-apply helm

23) Manually run provisioning (save the user) for the logins in step 2
