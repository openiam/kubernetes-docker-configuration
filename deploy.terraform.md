# Deploying OpenIAM with Terrform

This guide describes how to deploy OpenIAM with helm and terraform.

## Prerequisites

1. Install Terraform if it is not already installed (visit [terraform.io](https://terraform.io) for other distributions):

2. Modify terraform.tfvars.  Set the value of the "app_name" variable IN terraform.tfvars to be the same as the APP_NAME variable in env.sh


## Set common variables in terraform.tfvars

Below is a description of common variables, not specific to any cloud provider.  

| Variable Name                    | Required                         | Description   |
| -------------------------------- | -------------------------------- | ------------- |
| app_name                         | Y                                | This MUST be equal to the APP_NAME variable in env.sh
| helm_version                     | Y                                | Your helm version.  Should be equal to the output of `helm version -c --short | egrep -o 'v[0-9]*.[0-9]*.[0-9]*'`
| app_name                         | Y                                | A unique identifier of your application
| replica_count_map                | Y                                | This is a map that determines which app get how many replicas.  None of the values shoudl be greater than replica_count, or the total number of available nodes
| database.flywayBaselineVersion   | Y                                | Flyway baseline version.  set this to another value if you're
| database.flywayCommand           | Y                                | Flyway command to run.  `repair` or `migrate`
| database.type                    | Y                                | This is the type of database that you are using.  Can be one of 'MariaDB', 'Postgres', 'Oracle', or 'MSSQL'
| database.hibernate               | Y (when using Oracle or MSSQL)   | required for Oracle and MSSQL.  Ignored otherwise
|                                  |   N otherwise                    | For a complete list, see https://docs.jboss.org/hibernate/orm/3.5/javadocs/org/hibernate/dialect/package-summary.html
|                                  |                                  | Example values:
|                                  |                                  | org.hibernate.dialect.Oracle10gDialect ( the most common value for Oracle)
|                                  |                                  | org.hibernate.dialect.SQLServer2008Dialect (the most common value for MSSQL)               |
| database.root.password           | Y - in AWS                       | The root password to the database
|                                  | Y - when using OpenIAM's Mariadb |
|                                  |     or Postgres images           |
|                                  | N - otherwise                    |
| database.openiam.user            | Y                                | Username to the Openiam database
| database.openiam.password        | Y                                | Password to the Openiam database
| database.openiam.database_name   | Y                                | Openiam database name
| database.openiam.schema_name     | Y                                | Openiam schema name
| database.activiti.user           | Y                                | Username to the Activiti database
| database.activiti.password       | Y                                | Password to the Activiti database
| database.activiti.database_name  | Y                                | Activiti database name
| database.activiti.schema_name    | Y                                | Activiti schema name
| redis.password                   | Y                                | Redis password.  Can be blank
| rabbitmq.user                    | Y                                | RabbitMQ Username
| rabbitmq.password                | Y                                | RabbitMQ Password
| rabbitmq.cookie_name             | Y                                | RabbitMQ Cookie Name
| rabbitmq.memory.memoryHighWatermark             | Y                 | RabbitMQ Memory High Watermark
| rabbitmq.memory.request          | Y                                | RabbitMQ Requested Pod Memory
| rabbitmq.memory.limit            | Y                                | RabbitMQ Limit Pod Memory
| rabbitmq.serviceType             | Y                                | Service Type.  Set to `LoadBalancer` to expose externally, or set to `ClusterIP` to set internally.
| rabbitmq.tls.enabled             | Y                                | Is TLS Communication Enabled for RabbitMQ?  If yes, set to `true`.  Otherwise, set to `false`
| rabbitmq.tls.failIfNoPeerCert    | Y                                | When set to true, TLS RabbitMQ connection will be rejected if client fails to provide a certificate
| rabbitmq.tls.sslOptionsVerify    | Y                                | Should peer verification be enabled?  Default is `verify_peer`
| rabbitmq.jksKeyPassword          | Y                                | Password used to generate JKS file, if RabbitMQ TLS Communication is enabled
| logging.level.bash               | Y                                | Logging level of bash scripts.  TRACE and DEBUG are equivalent, and both force bash to run with the '-x' argument
| logging.level.app                | Y                                | for production use please change log level to warn or error. Debug will generate a lot of information.  possible values ERROR, WARN, INFO, DEBUG, or TRACE
| rproxy.https.disabled            | N                                | rproxy by default uses https. you can set this to 1 to disable https
| rproxy.https.protocol            | N                                | SSL Protocols options. if not set, apache 2.4 defaults will be used.
|                                  |                                  | For Example: disable everything except TLSv1.2 and allow only ciphers with high encryption:
|                                  |                                  | protocol="-ALL -TLSv1 -TLSv1.1 +TLSv1.2"
|                                  |                                  | cipherSuite="HIGH:!MEDIUM:!aNULL:!MD5:!RC4"
| rproxy.https.cipherSuite         | N                                | SSL Ciphersuite options. if not set, apache 2.4 defaults will be used.
|                                  |                                  | For Example: disable everything except TLSv1.2 and allow only ciphers with high encryption:
|                                  |                                  | protocol="-ALL -TLSv1 -TLSv1.1 +TLSv1.2"
|                                  |                                  | cipherSuite="HIGH:!MEDIUM:!aNULL:!MD5:!RC4"
| rproxy.https.host                | N                                | if https uses non-default(443) port, specify full https host name for redirects from http to https.
|                                  |                                  | for example:
|                                  |                                  | host=https://node1.openiam.com:8001
| rproxy.defaultUri                | Y                                | If user hit for example: http://demo.openiamdemo.com/ redirect him to
|                                  |                                  | http://demo.openiamdemo.com/selfservice/ by default, instead of showing error 404 (not found).
| rproxy.disableConfigure          | N                                | this is set to 1 by default. this allow to configure content provider on first access.
|                                  |                                  | But after that, it is possible to set it to 0 to disable ability to configure content provider using the /webconsole/setup url
| rproxy.verbose                   | N                                | rproxy debug options. set any value for enable or keep empty for disable debug logging
| rproxy.debug.base                | N                                | debug option.  Enable base debugging
| rproxy.debug.esb                 | N                                | debug option.  Enable debugging on calls to the ESB
| rproxy.debug.auth                | N                                | debug option.  Enable debugging on authentication calls
| rproxy.apache.extra              | N                                | File that is included in **httpd.conf** in the rproxy Docker container. Should be **extraApache.conf** or empty |
| rproxy.vhost.extra               | N                                | File that is included in **httpd.conf** in VirtualHost section in the rproxy Docker container. Should be **extraVHost.conf** or empty |
| rproxy.log.error                 | N                                | By default /dev/stderr used for OPENIAM_RPROXY_ERROR_LOG and
| rproxy.log.access                | N                                | /dev/stdout for OPENIAM_RPROXY_ACCESS_LOG. You can change that here,
|                                  |                                  | for example use /dev/stdout for both error log and access log
|                                  |                                  | or set access log to /dev/null and log only errors.
|                                  |                                  | if not set, defaults used.
| openiam.rproxy.deflate           | N                                | Apache mod_deflate compression ratio. Values from 0 to 9. By default is set to 6
| openiam.rproxy.csp               | N                                | Content-Security-Policy headers enabled by default. To disable set to '0'
| openiam.rproxy.cors              | N                                | Cross-Origin Resource Sharing headers enabled by default. To disable set to '0'
| autodeploy.openiam               | N                                | if true, the openiam stack will autodeploy on each terraform run
| autodeploy.rproxy                | N                                | if true, the openiam reverse proxy stack will autodeploy on each terraform run
| autodeploy.vault                 | N                                | if true, the openiam vault stack will autodeploy on each terraform run
| vault.cert.country               | N                                | Vault Certificate Country Code.  Ignored if you are using a custom vault certificate, as specified in the [parent](README.md) file.
| vault.cert.state                 | N                                | Vault Certificate Country State.  Ignored if you are using a custom vault certificate, as specified in the [parent](README.md) file.
| vault.cert.locality              | N                                | Vault Certificate Locality.  Ignored if you are using a custom vault certificate, as specified in the [parent](README.md) file.
| vault.cert.organization          | N                                | Vault Certificate Organiztion.  Ignored if you are using a custom vault certificate, as specified in the [parent](README.md) file.
| vault.cert.organizationunit      | N                                | Vault Certificate Organiztion Unit.  Ignored if you are using a custom vault certificate, as specified in the [parent](README.md) file.
| vault.vaultKeyPassword              | Y                                | Password used to generate the vault jks certificate.  Use a strong password.
| vault.secrets.javaKeystorePassword  | Y                                | Java keystore password
| vault.secrets.jks.password          | Y                                | OpenIAM JKS Encryption Password.   Use a strong password.
| vault.secrets.jks.keyPassword       | Y                                | OpenIAM JKS Key Password.  Use a strong password.
| vault.secrets.jks.cookieKeyPassword | Y                                | OpenIAM Cookie Key Password.  Use a strong password.
| vault.secrets.jks.commonKeyPassword | Y                                | OpenIAM Common Key Password.  Use a strong password.
| vault.replicas                      | Y                                | The number of vault and consul replicas
| vault.consul.storage                | Y                                | How much storage to provision to Consul PVC


## Set the Cloud Provider

Open the file 'main.tf' and change the following line:
1) if using Google Kubernetes Engine:
   ```
   source = "./modules/core/gke"
   ```

2) if using AWS:
      ```
      source = "./modules/core/aws"
      ```

3) if using Azure:
      ```
      source = "./modules/core/azure"
      ```

4) if deploying in-house:
      ```
      source = "./modules/core/helm"
      ```

## Set variables specific to your cloud provider      
Each cloud provider has it's own specific set of required and optional variables

Follow the guide based on your cloud provider:

1) [Goolge Cloud Engine](modules/core/gke/README.md)
2) [AWS](modules/core/aws/README.md)
3) [Azure](modules/core/azure/README.md)  
4) [Private Kuberenetes Cluster](modules/core/helm/README.md)


## Deploying

To deploy, run the following commands from the root folder of this project.  It may tak a while for all services to create and deploy

```
terraform init
terraform plan
terraform apply # enter 'yes' when asked to do so
```

Note:  our terraform scripts will change your ~/.kube/config file

Also note - if using AWS, you may have to run the above command twice.



## Destroying

Each cloud environment has it's own peculiarities (i.e. bugs) when destroying infrastructure.

Destroy your infrastructure based on your cloud provider:

1) [Goolge Cloud Engine](modules/core/gke/README.md)
2) [AWS](modules/core/aws/README.md)
3) [Azure](modules/core/azure/README.md)
4) [Private Kuberenetes Cluster](modules/core/helm/README.md)

## Managing State

Terraform stores state in local files

```
terraform.tfstate*
```

In addition, running against AWS and GKE modifies your ~/.kube/config file.

When switching environments, you MUST save these files off, so that you can return to previous environments.


## Migration guide

# pre-4.2.1 to 4.2.1

We've updated our vault datastore in 4.2.1.  To migrate, you will have to do the following:

1) Deploy with the `vault.migrate` terraform variable set to `true`
2) Wait for all pods to come up
3) Redeploy with the `vault.migrate` terraform variable set to `false`
4) Wait for all pods to come up
