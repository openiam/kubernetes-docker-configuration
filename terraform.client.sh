#!/usr/bin/env bash

set -x
set -e

echo "This script uses or saves off terraform scripts, so that you can switch between clients and/or environments"

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]; then
    echo "Usage:  ./terraform.client.sh save|use [root_directory_where_to_save_files] [client_name] [environment]"
    echo "Example: ./terraform.client.sh ~/dev/terraform awesomeClient prod"
    exit 1;
fi

path="$2/$3/$4"
mkdir -p "$path/openiam"

case $1 in
   "save")
        cp -r env.sh $path/
        cp -r *.tfvars $path/
        cp -r terraform.tfstate* $path/
        cp -r main.tf $path/
        cp -r openiam/values.yaml $path/openiam/
        cp -r .apache $path/
        cp -r .deploy $path/
        cp -r .ssl $path/
        cp -r .vault $path/
        cp -r config $path/
        cp -r .kube-config.yaml $path/
        cp -r openiam-configmap $path/
        ;;
   "use")
        cp -r $path/env.sh .
        cp -r $path/*.tfvars .
        cp -r $path/terraform.tfstate* .
        cp -r $path/main.tf .
        cp -r $path/openiam/values.yaml openiam/values.yaml
        cp -r $path/.apache .
        cp -r $path/.deploy .
        cp -r $path/.ssl .
        cp -r $path/.vault .
        cp -r $path/config .
        cp -r $path/.kube-config.yaml .
        cp -r $path/openiam-configmap .
        ;;
   *)
        echo "Usage:  ./terraform.client.sh save|use [root_directory_where_to_save_files] [client_name] [environment]"
        exit 1;
        ;;
esac