#!/bin/bash

set -e
set -x

sudo apt-get install -y --no-install-recommends curl bash git wget unzip zip openssh-client

#jfrog
sudo curl -fL https://getcli.jfrog.io | sudo sh
sudo chmod a+x jfrog
sudo cp jfrog /usr/local/bin


# Add Helm's GPG key
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -

# Add the Helm repository
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list

# Update package lists
sudo apt-get update

# Install Helm
sudo apt-get install helm