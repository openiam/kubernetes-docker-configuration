#!/usr/bin/env bash

set -x
set -e

mkdir -p .state
cp terraform.tfstate* .state/
cp config* .state/
cp kubeconfig* .state/
cp env.sh .state/
cp vault* .state/
cp .kube-config.yaml .state/
cp terraform.tfvars .state/