# Elasticsearch TLS Directory

This directory should contain required SSL-specific files for elasticsearch

1) `elasticsearch.crt` - Server certificate content
2) `elasticsearch.key` - Server private key content
4) `elasticsearch.ca.crt` - Certificate Authority (CA) bundle content


These files must  go into the root level directory `.elasticsearch` and `openiam-configmap/.elasticsearch`
