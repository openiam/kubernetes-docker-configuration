
locals {
    hostname   = azurerm_redis_cache.cache.hostname
    port       = azurerm_redis_cache.cache.port
    password   = azurerm_redis_cache.cache.primary_access_key
 }


output "redis_port" {
  value       = local.port
  description = "The port of the created redis instance"
}

output "redis_host" {
  value       = local.hostname
  description = "The host of the created redis instance"
}

output "redis_password" {
  value = local.password
  description = "Redis password"
}

output "redis_mode" {
  value = "single"
  description = "Redis Mode"
}
