terraform {
  required_version = ">= 0.12.21"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.56.0"
    }
  }
}

provider "azurerm" {
  features {}
}

#---------------------------------------------------------
# Resource Group Creation or selection - Default is "false"
#----------------------------------------------------------
data "azurerm_resource_group" "rgrp" {
  count = var.context.create_resource_group == false ? 1 : 0
  name  = var.context.resource_group_name
}

resource "azurerm_resource_group" "rg" {
  count    = var.context.create_resource_group ? 1 : 0
  name     = var.context.resource_group_name
  location = var.context.region
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.context.infra.azure.virtual_network_name
  location            = var.context.region
  resource_group_name = var.context.resource_group_name
  address_space       = [ var.context.infra.azure.virtual_network_cidr ]
}

data "azurerm_virtual_network" "vnet" {
  name                = azurerm_virtual_network.vnet.name
  resource_group_name = var.context.resource_group_name
}

resource "azurerm_subnet" "aks" {
  name                 = "snet-openiam-aks"
  resource_group_name  = var.context.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [ var.context.infra.azure.aks_subnet_cidr ]
  service_endpoints    = ["Microsoft.Sql"]
}

resource "azurerm_subnet" "database" {
  name                 = "snet-openiam-database"
  resource_group_name  = var.context.resource_group_name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [ var.context.infra.azure.db_subnet_cidr ]
  service_endpoints    = ["Microsoft.Sql"]
}

module "database" {
  source     = "../../infrastructure/database/azure"
  context    = var.context
}

# This link provides AKS pods with access to the private database endpoint
resource "azurerm_private_dns_zone_virtual_network_link" "opemiam_vnet" {
  name                  = "link-to-openiam-vnet"
  resource_group_name   = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
  private_dns_zone_name = module.database.private_database_link_name
  virtual_network_id    = azurerm_virtual_network.vnet.id
}

data "azurerm_virtual_network" "deploy" {
  name                = var.context.infra.azure.deploy_vnet_name
  resource_group_name = var.context.resource_group_name
}

resource "azurerm_virtual_network_peering" "deploy_to_aks" {
  name                         = "peer-to-vnet-aks"
  resource_group_name          = var.context.resource_group_name
  virtual_network_name         = var.context.infra.azure.deploy_vnet_name
  remote_virtual_network_id    = azurerm_virtual_network.vnet.id
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = true
}

resource "azurerm_virtual_network_peering" "aks_to_deploy" {
  name                         = "peer-to-vnet-deploy"
  resource_group_name          = var.context.resource_group_name
  virtual_network_name         = azurerm_virtual_network.vnet.name
  remote_virtual_network_id    = data.azurerm_virtual_network.deploy.id
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
}
 
resource "azurerm_kubernetes_cluster" "aks" {
  name                    = var.context.app_name
  location                = var.context.region
  resource_group_name     = var.context.resource_group_name
  dns_prefix              = var.context.app_name
  private_cluster_enabled = true
  kubernetes_version      = var.context.kubernetes.aks.version
  
  default_node_pool {
    name                 = "openiampool"
    vm_size              = var.context.kubernetes.aks.machine_type
    node_count           = var.context.replica_count
    orchestrator_version = var.context.kubernetes.aks.version
    availability_zones   = ["1", "2", "3"]
    os_disk_size_gb      = 128
    vnet_subnet_id       = azurerm_subnet.aks.id
    enable_auto_scaling  = false
    max_count            = null
    min_count            = null
  }

  identity {
    type         = "SystemAssigned"
   }
  
  network_profile {
    network_plugin = "azure"
  }
  
  role_based_access_control { 
    enabled = true
  }
  depends_on = [
    module.database,azurerm_subnet.aks,
  ]
}

###### This is to perform the network link from the Private DNS zone that is automatically created by Microsoft to the 
###### virtual network where deployment machine is to allow kubectl and helm to communicate with k8
locals {
  private_dns_zone_name = join(".", slice(split(".", azurerm_kubernetes_cluster.aks.private_fqdn), 1, length(split(".", azurerm_kubernetes_cluster.aks.private_fqdn))))
}

resource "azurerm_private_dns_zone_virtual_network_link" "zone_link_to_hub_vnet" {
  name                  = "link-to-${var.context.infra.azure.deploy_vnet_name}"
  resource_group_name   = "MC_${var.context.resource_group_name}_${var.context.app_name}_${var.context.region}"
  private_dns_zone_name = local.private_dns_zone_name
  virtual_network_id    = data.azurerm_virtual_network.deploy.id
}
######
######

resource "null_resource" "update_kubeconfig" {
  provisioner "local-exec" {
    command = <<EOT
      az aks get-credentials --resource-group ${var.context.resource_group_name} --overwrite-existing --name ${var.context.app_name};
   EOT
  }
  depends_on = [
    azurerm_kubernetes_cluster.aks
  ]
}

data "azurerm_virtual_network" "deploy_vnet" {
  name                = var.context.infra.azure.deploy_vnet_name
  resource_group_name = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
}

module "helm" {
  source = "../../helm"
  kube_host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
  kube_token                  = azurerm_kubernetes_cluster.aks.kube_config.0.password
  kube_cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
  context                     = var.context
  cloud_provider              = "azure"
  depends                     = [ azurerm_private_dns_zone_virtual_network_link.zone_link_to_hub_vnet.id ]
}

module "elasticsearch" {
  source = "../../infrastructure/elasticsearch/helm"
  kube_host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
  kube_token                  = azurerm_kubernetes_cluster.aks.kube_config.0.password
  kube_client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
  kube_client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
  kube_cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
  context                     = var.context
  depends                     = [ azurerm_private_dns_zone_virtual_network_link.zone_link_to_hub_vnet.id ]
}

module "redis" {
  source     = "../../infrastructure/redis/azure"
  context    = var.context
  depends    = [ azurerm_virtual_network.vnet.id ]
}

module "gremlin" {
  source        = "../../infrastructure/gremlin/helm"
  context       = var.context
  kube_host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
  kube_token                  = azurerm_kubernetes_cluster.aks.kube_config.0.password
  kube_cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
  elasticsearch = {
    host = "${module.elasticsearch.elasticsearch_host}"
    port = "${module.elasticsearch.elasticsearch_port}"
  }
  depends                     = [ azurerm_private_dns_zone_virtual_network_link.zone_link_to_hub_vnet.id ]
}

module "openiam-app" {
  source = "../../app"
  kube_host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
  kube_token                  = azurerm_kubernetes_cluster.aks.kube_config.0.password
  kube_client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
  kube_client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
  kube_cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
  
  context = var.context
  depends                     = [ azurerm_private_dns_zone_virtual_network_link.zone_link_to_hub_vnet.id ]
  
  database = {
    host = "${module.database.database_host}"
    port = "${module.database.database_port}"
    created_database = "${module.database.database_name}"
    flywayBaselineVersion = "${var.context.database.flywayBaselineVersion}"
    flywayCommand = "${var.context.database.flywayCommand}"
  }

  elasticsearch = {
    host = "${module.elasticsearch.elasticsearch_host}"
    port = "${module.elasticsearch.elasticsearch_port}"
    kibana_full_url = ""
  }
  redis = {
    host = "${module.redis.redis_host}"
    port = "${module.redis.redis_port}"
    password = "${module.redis.redis_password}"
    mode = "${module.redis.redis_mode}"
  }

  rabbitmq = {
    user = "${var.context.rabbitmq.user}"
    password = "${var.context.rabbitmq.password}"
    host = "${module.helm.rabbitmq_hostname}"
    cookie_name = "${var.context.rabbitmq.cookie_name}"
  }

  vault = {
    host = "${module.helm.vault_hostname}"
  }

  gremlin = {
    host = "${module.gremlin.host}"
    port = "${module.gremlin.port}"
  }
  cloud_provider = "helm"
}

