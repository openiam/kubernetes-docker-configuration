provider "helm" {
  version = "~> 2.0.0"

  kubernetes {
    config_path            = "~/.kube/config"
    host                   = "${var.kube_host}"
    token                  = "${var.kube_token}"
    client_certificate     = "${var.kube_client_certificate}"
    client_key             = "${var.kube_client_key}"
    cluster_ca_certificate = "${var.kube_cluster_ca_certificate}"
  }
}

resource "null_resource" "depends_on" {
  triggers = {
    depends_on = "${join("", var.depends)}"
  }
}

resource "helm_release" "elasticsearch" {
  name      = "${var.context.app_name}-elasticsearch"
  repository = "${var.context.mods.elasticsearch}"
  chart      = "elasticsearch"
  namespace = "${var.context.namespace}"
  version    = "7.17.3"
  recreate_pods = true
  wait       = "false"
  depends_on    = [null_resource.depends_on]
  
  values = [
    "${file(".deploy/elasticsearch.values.yaml")}"
  ]

  set {
      name = "openiam.appname"
      value = "${var.context.app_name}"
  }

  set {
    name = "esJavaOpts"
    value = "${var.context.elasticsearch.helm.esJavaOpts}"
  }

  set {
    name = "replicas"
    value = "${var.context.elasticsearch.helm.replicas}"
    type = "string"
  }

  set {
    name = "clusterHealthCheckParams"
    value = "wait_for_status=yellow&timeout=10s"
  }

  set {
    name = "volumeClaimTemplate.resources.requests.storage"
    value = "${var.context.elasticsearch.helm.storageSize}"
  }
}
