# Azure Kubernetes Guide

Follow this guide to deploy OpenIAM in Azure

## Preparation. Set up the deployment environment

1. Prepare a deployment virtual machine in the Azure (below are all examples for Ubuntu). 
   Openiam installation will be performed from the deployment machine.
   A VM, virtual network and subnet have to be created in a subscription of the Azure manually. Their values will be used by TF.

  - install a VM (Ubuntu) in the Azure (can be configuration with minimal system resources) 
  - install and configure azure cli on the VM [azure cli ](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli)
    
    ```
    curl -L https://aka.ms/InstallAzureCli | bash
    $ az login
    ```

  - download terraform 0.12.21 to the VM [download terraform ](https://releases.hashicorp.com/terraform/0.12.21/) 
    and install following [instruction](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
    or just run commands below:

    ```
    $ curl https://releases.hashicorp.com/terraform/0.12.21/terraform_0.12.21_linux_amd64.zip -o ./terraform_0.12.21_linux_amd64.zip
    $ unzip ./terraform_0.12.21_linux_amd64.zip
    $ mv ./terraform /usr/bin/
    $ terraform version     (make sure you have installed proper version)
    ```

  - install heln on the VM 
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh
    ```
  - install kubectl on the VM
    ```
    $ curl -LO "https://dl.k8s.io/release/1.25.6/bin/linux/amd64/kubectl"
    $ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    ```

  - install pgsql client on the VM (Optional) if you are going to use Azure Database for PostgreSQL servers (not need if use Azure MariaDB or Azure SQL)
    ```
    sudo apt-get install -y postgresql-client
    ```

  - Install the SQL Server command-line tools sqlcmd on the VM (Optional) if you are going to use Azure SQL server. See the [doc](https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?tabs=ubuntu-install%2Credhat-offline&view=sql-server-ver16#ubuntu)
    ```
    $ curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
    $ curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
    $ sudo apt-get update
    $ sudo apt-get install mssql-tools unixodbc-dev
    $ echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
    ```


## Install

1. Configure terraform: 
   Set the region variable in terraform.tfvars use below command to find region name (Use column "Name" to fill terraform value)

  ```
  $ az account list-locations -o table
  ```
2. Set the Azure-specific variables in terraform.tfvars



| Variable Name                                    | Required                       | Default Value               |  Description   |
| ------------------------------------- | ------------------------------ | --------------------------- | ------------- |
| region                                           | Y                              |                             | The region to be deployed.  For example, eastus
| replica_count                                    | Y                              |                             | The total number of nodes to be created in the kubernetes cluster
| create_resource_group                            | Y                              | false                       | Either to create the Azure Resource Group or use existing
| resource_group_name                              | Y                              | OpeniamGroup                | The Azure Resource Group Name to be created or to be used to deploy all reources
| database.root.user                               | Y                              |                             | The root username to the database
| database.root.password                           | Y                              |                             | The root password to the database
| database.port                                    | Y                              |                             | Database port.
| database.azure.sku_name                          | Y                              | GP_Gen5_2                   | The Azure Database sku_name
| database.azure.storage_mb                        | Y                              | 20480                       | The azure database storage size in Mb
| database.azure.backup_retention_days             | Y                              | 7                           | The Azure backup retention period
| infra.azure.virtual_network_name                 | Y                              | vnet-openiam                | The Virtual Network name to be created
| infra.azure.virtual_network_cidr                 | Y                              | 10.1.0.0/16                 | The Virtual Network CIDR to be created
| infra.azure.aks_subnet_cidr                      | Y                              | 10.1.0.0/24                 | The subnet CIDR the AKS to be created in
| infra.azure.db_subnet_cidr                       | Y                              | 10.1.1.0/24                 | The subnet CIDR the database to be created in
| infra.azure.redis_subnet_cidr                    | Y                              | 10.1.2.0/24                 | The subnet CIDR the redis to be created in
| infra.azure.deploy_vnet_name                     | Y                              | hub-vnet                    | The Virtual network name the deploymnet machine belongs to (have to be created manually on deployment environment preparation stage, look at Paragraph 1)
| infra.azure.deploy_resource_group_name           | N                              |                             | The Azure Resource Group Name the deployment VM were created in, leave empty if you are going to deploy all resources in the same group
| infra.azure.deploy_subnet_name                   | Y                              | jumpboxSubnet               | The subnet name the deployment machine belongs to (have to be created manually on deployment environment preparation stage, look at Paragraph 1)
| redis.azure.capacity                             | Y                              | 1                           | The size of the Redis cache to deploy. Valid values for a SKU Premium are 1, 2, 3, 4 (1 = 6Gb, 2 = 13Gb, 3 =26Gb, 4 = 53Gb)


3. Run the apply command

```
$ terraform init
$ terraform apply # enter 'yes' when asked to do so
```

The deployment process takes about 20-30 min.

## Destroying

1. Run the destroy command:

```
terraform destroy # enter 'yes' when asked to do so
```

2. Delete terraform's state file:
```
rm -rf terraform.tfstate*
```
