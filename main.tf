variable "region" {}
variable "create_resource_group" {}
variable "resource_group_name" {}
variable "app_name" {}
variable "use_longhorn" {}
variable "replica_count" {}
variable "replica_count_map" {}
variable "database" {}
variable "infra" {}
variable "redis" {}
variable "rabbitmq" {}

variable "elasticsearch" {}

variable "logging" {}

variable "rproxy" {}

variable "iam" {}

variable "autodeploy" {}

variable "kubernetes" {}

variable "kibana" {}

variable "metricbeat" {}

variable "filebeat" {}

variable "gremlin" {}

variable "vault" {}

variable "namespace" {}

variable "mods" {}

variable "cassandra" {}

variable "cluster" {}

variable "javaOpts" {}

variable "stash" {}

module "deployment" {
  source = "./modules/core/helm"
  context = {
    namespace = "${var.namespace}"
    region = "${var.region}"
    create_resource_group = "${var.create_resource_group}"
    resource_group_name = "${var.resource_group_name}"
    app_name = "${var.app_name}"
    use_longhorn = var.use_longhorn
    replica_count = "${var.replica_count}"
    replica_count_map = var.replica_count_map
    infra = var.infra
    database = var.database
    redis = var.redis
    rabbitmq = var.rabbitmq
    logging = var.logging
    elasticsearch = var.elasticsearch
    rproxy = var.rproxy
    iam = var.iam
    autodeploy = var.autodeploy
    kubernetes = var.kubernetes
    kibana = var.kibana
    metricbeat = var.metricbeat
    filebeat = var.filebeat
    gremlin = var.gremlin
    vault = var.vault
    mods = var.mods
    cassandra = var.cassandra
    cluster = var.cluster
    javaOpts = var.javaOpts
    stash = var.stash
  }
}


