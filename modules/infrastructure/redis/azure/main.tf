resource "null_resource" "depends_on" {
  triggers = {
    depends_on = "${join("", var.depends)}"
  }
}

resource "azurerm_subnet" "cache" {
  name                 = "snet-openiam-cache"
  resource_group_name  = var.context.resource_group_name
  virtual_network_name = var.context.infra.azure.virtual_network_name
  address_prefixes     = [ var.context.infra.azure.redis_subnet_cidr ]
  depends_on           = [ null_resource.depends_on ]
}

resource "azurerm_redis_cache" "cache" {
  name                          = var.context.app_name
  location                      = var.context.region
  resource_group_name           = var.context.resource_group_name
  capacity                      = var.context.redis.azure.capacity
  family                        = "P"
  sku_name                      = "Premium"
  subnet_id                     = azurerm_subnet.cache.id
  enable_non_ssl_port           = true
  minimum_tls_version           = "1.2"
  public_network_access_enabled = false
  redis_configuration {
    enable_authentication       = false
  }
}
