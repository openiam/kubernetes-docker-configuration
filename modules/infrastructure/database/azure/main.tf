locals {
  server_name             = lower(var.context.database.azure.type) == "postgres" ? data.azurerm_postgresql_server.db.0.fqdn : lower(var.context.database.azure.type) == "mssql" ? data.azurerm_mssql_server.db.0.fully_qualified_domain_name : ""
  pg_init_script          = templatefile("${path.module}/templates/postgres-init.tfpl", { JDBC_OPENIAM_DB_USER = var.context.database.openiam.user, JDBC_OPENIAM_DB_PASSWORD = var.context.database.openiam.password, JDBC_ACTIVITI_DB_USER = var.context.database.activiti.user, JDBC_ACTIVITI_DB_PASSWORD = var.context.database.activiti.password, OPENIAM_DATABASE_NAME = var.context.database.openiam.database_name, ACTIVITI_DATABASE_NAME = var.context.database.activiti.database_name, ADMIN_NAME = var.context.database.root.user })
  ms_init_master_script   = templatefile("${path.module}/templates/mssql-init-master.tfpl", { JDBC_OPENIAM_DB_USER = var.context.database.openiam.user, JDBC_OPENIAM_DB_PASSWORD = var.context.database.openiam.password, JDBC_ACTIVITI_DB_USER = var.context.database.activiti.user, JDBC_ACTIVITI_DB_PASSWORD = var.context.database.activiti.password })
  ms_init_openiam_script  = templatefile("${path.module}/templates/mssql-init-openiam.tfpl", { JDBC_OPENIAM_DB_USER = var.context.database.openiam.user })
  ms_init_activiti_script = templatefile("${path.module}/templates/mssql-init-activiti.tfpl", { JDBC_ACTIVITI_DB_USER = var.context.database.activiti.user })
}


##  THESE RESOURCES ARE FOR PROVIDING THE DEPLOYMENT MACHINE WITH CONNECTION TO THE PRIVATE DATABASE
data "azurerm_virtual_network" "deploy_vnet" {
  name                = var.context.infra.azure.deploy_vnet_name
  resource_group_name = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
}

data "azurerm_subnet" "deploy_box" {
  name                 = var.context.infra.azure.deploy_subnet_name
  virtual_network_name = data.azurerm_virtual_network.deploy_vnet.name
  resource_group_name  = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
}

# Create endpoint in the deploy hub subnet for db
resource "azurerm_private_endpoint" "db" {
  name                = "openiam-db-endpoint"
  location            = var.context.region
  resource_group_name = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
  subnet_id           = data.azurerm_subnet.deploy_box.id

  private_service_connection {
    name                           = "db-connection"
    private_connection_resource_id = lower(var.context.database.azure.type) == "mariadb" ? azurerm_mariadb_server.db.0.id : lower(var.context.database.azure.type) == "mssql" ? azurerm_mssql_server.db.0.id : lower(var.context.database.azure.type) == "postgres" ? azurerm_postgresql_server.db.0.id : ""
    subresource_names              = lower(var.context.database.azure.type) == "mariadb" ? ["mariadbServer"] : lower(var.context.database.azure.type) == "mssql" ? ["sqlServer"] : lower(var.context.database.azure.type) == "postgres" ? ["postgresqlServer"] : [""]
    is_manual_connection           = false
  }

  private_dns_zone_group {
    name                 = "default"
    private_dns_zone_ids = [azurerm_private_dns_zone.db.id]
  }
}

resource "azurerm_private_dns_zone" "db" {
  name                = lower(var.context.database.azure.type) == "mariadb" ? "privatelink.mariadb.database.azure.com" : lower(var.context.database.azure.type) == "mssql" ? "privatelink.database.windows.net" : lower(var.context.database.azure.type) == "postgres" ? "privatelink.postgres.database.azure.com" : ""
  resource_group_name = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
}

data "azurerm_private_dns_zone" "db" {
  name                = azurerm_private_dns_zone.db.name
  resource_group_name = var.context.resource_group_name
}

resource "azurerm_private_dns_zone_virtual_network_link" "deploy_vnet" {
  name                  = "link-to-deploy-vnet"
  resource_group_name   = var.context.infra.azure.deploy_resource_group_name == "" ? var.context.resource_group_name : var.context.infra.azure.deploy_resource_group_name 
  private_dns_zone_name = azurerm_private_dns_zone.db.name
  virtual_network_id    = data.azurerm_virtual_network.deploy_vnet.id
}
