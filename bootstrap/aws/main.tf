variable "region" {}

terraform {
  required_version = ">= 0.12.21"
}

provider "aws" {
  version = "~> 3.37.0"
  region  = "${var.region}"
}

resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
  description = "Managed by Terraform"
}
